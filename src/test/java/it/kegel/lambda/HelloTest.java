package it.kegel.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class HelloTest {

    private Context context = new TestContext();


    @Test
    public void testHandleRequest() {
        Hello hello = new Hello();
        String result = hello.handleRequest("Hello", context);
        assertEquals("Response to 'Hello'", result);
    }

    @Test
    @Ignore
    public void testHandleRequestWithRestCall() {
        Hello hello = new Hello();
        String result = hello.handleRequest("version", context);
        assertEquals("1.6.1", result);
    }
}

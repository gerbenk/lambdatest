package it.kegel.lambda;

import com.amazon.speech.speechlet.*;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.SimpleCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class HomeEasySpeechlet  implements Speechlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeEasySpeechlet.class);

    @Override
    public void onSessionStarted(SessionStartedRequest request, Session session) throws SpeechletException {
        replyWith("The session was started");
    }

    @Override
    public SpeechletResponse onLaunch(LaunchRequest request, Session session) throws SpeechletException {
        return replyWith("Hello, my name is Eric and I am very wise.");
    }

    @Override public SpeechletResponse onIntent(final IntentRequest request, final Session session) {
        LOGGER.info("onIntent requestId={}, sessionId={}",request.getRequestId(),session.getSessionId());

        boolean sw = "on".equalsIgnoreCase(request.getIntent().getSlot("Switch").getValue());

        try {
            HttpHelper.get("http://home.kegel.it:8084/1/1/" + (sw ? "on" : "off"));
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return replyWith("Okikadoki");
    }

    @Override
    public void onSessionEnded(SessionEndedRequest request, Session session) throws SpeechletException {
        replyWith("The session was ended");
    }

    private SpeechletResponse replyWith(String speechText) {

        // Create the Simple card content.
        SimpleCard card = new SimpleCard();
        card.setTitle("Response");
        card.setContent(speechText);

        // Create the plain text output.
        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechText);

        return SpeechletResponse.newTellResponse(speech, card);
    }
}

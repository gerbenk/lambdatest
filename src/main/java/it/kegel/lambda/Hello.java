package it.kegel.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.io.IOException;

public class Hello implements RequestHandler<String, String> {

    @Override
    public String handleRequest(String input, Context context) {
        LambdaLogger logger = context.getLogger();
        logger.log("received: " + input);

        switch (input) {
            case "version":
                return getParcel4meApiVersion();
            default:
                return "Response to '" + input + "'";
        }
    }

    private String getParcel4meApiVersion() {
        String url = "https://dev.parcel4me.nl/api/version";
        try {
            return new HttpHelper().post(url, "");
        } catch (IOException e) {
            return null;
        }
    }
}
package it.kegel.lambda;

import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler;

import java.util.HashSet;
import java.util.Set;

public class OrsouwaanseQuoteSpeechletRequestStreamHandler extends SpeechletRequestStreamHandler {

    private static final Set<String> supportedApplicationIds = new HashSet<>();

    static {
        String appId = System.getenv("APP_ID");
        supportedApplicationIds.add(appId);
    }

    public OrsouwaanseQuoteSpeechletRequestStreamHandler() {
        super(new OrsouwaanseQuoteSpeechlet(), supportedApplicationIds);
    }
}

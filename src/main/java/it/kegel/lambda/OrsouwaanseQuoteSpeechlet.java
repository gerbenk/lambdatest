package it.kegel.lambda;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.*;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.SimpleCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class OrsouwaanseQuoteSpeechlet implements Speechlet {

    private static final Logger log = LoggerFactory.getLogger(OrsouwaanseQuoteSpeechlet.class);

    private String[] quotes = {
            "Tomorrow free beer",
            "In the name of gods",
            "intunen on my directory",
            "No, that was before",
            "habbi babbi",
            "check",
            "Hatzikidee, delicious bytes",
            "Hey, new brackets",
            "Nothing is impossible for the who doesn't have to do the work.",
            "pooing without pressing",
            "so to speak",
            "is he or something a meat eater",
            "back to the order of the day",
            "hold your backlog",
            "lick my bitrate",
            "make it so",
            "not bothered by any knowledge",
            "look..., now we're talking",
            "Ask me if I care?",
            "You should not make things easier than they are.",
            "Ham and eggs",
            "It is just pure madness",
            "Goddamn motherfuckers. Punk-ass nigga’s.",
            "Do me a banana, said Aunt Sjaantje",
            "It's all new, I need old crap",
            "bitte warten",
            "When it doesn't do what it should..., control-alt-reboot",
            "That's a fine mess I got myself into",
            "Whatever's happinging here is technically impossible.",
            "It shal be a sausage to me, so to speak",
            "Who has come up with this, has to put the cock on the chopping block",
            "That shoots not up",
            "There's something fishy here"
    };


    @Override
    public void onSessionStarted(SessionStartedRequest request, Session session) throws SpeechletException {
        getHelloResponse("The session was started");
    }

    @Override
    public SpeechletResponse onLaunch(LaunchRequest request, Session session) throws SpeechletException {
        return getHelloResponse("Hello, my name is Eric and I am very wise.");
    }

    @Override public SpeechletResponse onIntent(final IntentRequest request, final Session session) {
        log.info("onIntent requestId={}, sessionId={}",request.getRequestId(),session.getSessionId());

        String quote = quotes[(int) (Math.random() * quotes.length)];

        return getHelloResponse(quote);
    }

    @Override
    public void onSessionEnded(SessionEndedRequest request, Session session) throws SpeechletException {
        getHelloResponse("The session was ended");
    }

    private SpeechletResponse getHelloResponse(String speechText) {


        // Create the Simple card content.
        SimpleCard card = new SimpleCard();
        card.setTitle("Response");
        card.setContent(speechText);

        // Create the plain text output.
        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechText);

        return SpeechletResponse.newTellResponse(speech, card);
    }


}

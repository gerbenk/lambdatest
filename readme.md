# lambdatest

Just an example project how to write an AWS lambda function in Java and
how to deploy it using Gradle.

[![Known Vulnerabilities](https://snyk.io/test/github/gerbenk/lambdatest/badge.svg?targetFile=build.gradle)](https://snyk.io/test/github/gerbenk/lambdatest?targetFile=build.gradle)
